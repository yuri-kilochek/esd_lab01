#!/bin/env/python
# coding: utf-8

import argparse
import yaml
import sys

###############################################################################

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument('knowledge', type=argparse.FileType('r'))
argument_parser.add_argument('-d', '--depth', type=int, default=10)
args = argument_parser.parse_args()

###############################################################################

knowledge = yaml.load(args.knowledge)

variable_count = 0
new_knowledge = []
for production in knowledge:
    substitutions = {}
    new_production = []
    for term in production:
        new_term = [term[0]]
        for symbol in term[1:]:
            if isinstance(symbol, str) and symbol.startswith('$'):
                new_symbol = substitutions.get(symbol)
                if new_symbol is None:
                    new_symbol = f'${variable_count}'
                    variable_count += 1
                    substitutions[symbol] = new_symbol
                symbol = new_symbol
            new_term.append(symbol)
        term = new_term[0], tuple(new_term[1:])
        new_production.append(term)
    production = new_production[0], tuple(new_production[1:])
    new_knowledge.append(production)
knowledge = tuple(new_knowledge)

###############################################################################

def unify(a, b):
    a_predicate, a_parameters = a
    b_predicate, b_parameters = b
    if a_predicate != b_predicate:
        return None
    if len(a_parameters) != len(b_parameters):
        return None
    substitutions = {}
    for a_parameter, b_parameter in zip(a_parameters, b_parameters):
        if b_parameter.startswith('$'):
            if substitutions.get(b_parameter, a_parameter) != a_parameter:
                return None
            substitutions[b_parameter] = a_parameter
            continue
        if a_parameter.startswith('$'):
            if substitutions.get(a_parameter, b_parameter) != b_parameter:
                return None
            substitutions[a_parameter] = b_parameter
            continue
        if a_parameter != b_parameter:
            return None
    return substitutions

def apply(substitutions, term):
    return term[0], tuple(substitutions.get(s, s) for s in term[1])

def ask(question, depth):
    all_answers = set()
    for statement, conditions in knowledge:
        substitutions = unify(question, statement)
        if substitutions is None:
            continue
        states = {(apply(substitutions, question), conditions)}
        for _ in conditions:
            new_states = set()
            for answer, conditions in states:
                subquestion = apply(substitutions, conditions[0])
                subanswers = ask(subquestion, depth - 1)
                for subanswer in subanswers:
                    backsubstitutions = unify(subanswer, subquestion)
                    new_answer = apply(backsubstitutions, answer)
                    new_conditions = []
                    for condition in conditions[1:]:
                        condition = apply(backsubstitutions, condition)
                        new_conditions.append(condition)
                    new_conditions = tuple(new_conditions)
                    new_states.add((new_answer, new_conditions))
            states = new_states
        for answer, _ in states:
            all_answers.add(answer)
    return all_answers

###############################################################################

questions = yaml.load(sys.stdin)

new_questions = []
for question in questions:
    question = question[0], tuple(question[1:])
    new_questions.append(question)
questions = tuple(new_questions)

###############################################################################

answers = []
for question in questions:
    question_answers = ask(question, args.depth)
    answers.append((question, frozenset(question_answers)))

###############################################################################

new_answers = []
for question, question_answers in answers:
    question = [question[0], *question[1]]
    question_answers = [[a[0], *a[1]] for a in question_answers]
    new_answers.append([question, *question_answers])
answers = new_answers

yaml.dump(answers, sys.stdout)
